<?php
require_once('../core/config.php');
require_once('../core/head.php');
$thongbao = null;
session_start();
if (isset($_SESSION['logger']['username'])) {
    echo '<script>window.location.href = "/";</script>';
    exit();
}
if (isset($_POST['submit']) && $_POST['username'] != '' && $_POST['password'] != '') {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $captcha = $_POST['g-recaptcha-response'];
    if (isset($_GET['referral'])) {
        $referral = $_GET['referral'];
    } else {
        $referral = "";
    }

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }

    $sql = "SELECT ip_address FROM account ac JOIN referral rf ON ac.id = rf.id_account WHERE rf.referral = '$referral'";
    $result = $config->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $ipAddressReferral = $row['ip_address'];
    } else {
        $ipAddressReferral = null;
    }


    if (!$captcha) {
        $thongbao = '<span style="color: red; font-size: 12px; font-weight: bold;">Hãy xác minh captcha!</span>';
    } else {
        $sql = "SELECT*FROM account WHERE username = '$username'";
        $old = mysqli_query($config, $sql);
        if (mysqli_num_rows($old) > 0) {
            $thongbao = '<span style="color: red; font-size: 12px; font-weight: bold;">Tài khoản đã tồn tại!</span>';
        } else {
            if ($ip_address == $ipAddressReferral) {
                $sql = "INSERT INTO account (username,password) VALUES ('$username','$password')";
                mysqli_query($config, $sql);
                $thongbao = '<span style="color: green; font-size: 12px; font-weight: bold;">Đăng ký thành công! Nhưng mã giới thiệu không hợp lệ vì cùng ip</span>';
                // sleep(5);
                // echo '<script>window.location.href = "/pages/dangnhap.php";</script>';
            } else {
                $sql = "INSERT INTO account (username,password, referral) VALUES ('$username','$password', '$referral')";
                mysqli_query($config, $sql);
                $thongbao = '<span style="color: green; font-size: 12px; font-weight: bold;">Đăng ký thành công!</span>';
                // sleep(2);
                // echo '<script>window.location.href = "/pages/dangnhap.php";</script>';
            }
        }
    }
}
?>
<main>
    <div style="background: #ffe8d1; border-radius: 7px; box-shadow: 0px 2px 5px black;" class="pb-1">
        <form class="text-center col-lg-5 col-md-10" style="margin: auto;" method="post" action="">
            <h1 class="h3 mb-3 font-weight-normal">Đăng Ký Tài Khoản</h1>
            <?= $thongbao; ?>
            <input style="height: 50px; border-radius: 15px; font-weight: bold;" name="username" required="" autofocus="" type="text" class="form-control mt-1" placeholder="Tên tài khoản">
            <span style="color: red; font-size: 12px; font-weight: bold;">
            </span>
            <input style="height: 50px; border-radius: 15px; font-weight: bold;" name="password" required="" type="password" class="form-control mt-1" placeholder="Mật khẩu">
            <span style="color: red; font-size: 12px; font-weight: bold;">
            </span>
            <!-- <input style="height: 50px; border-radius: 15px; font-weight: bold;" name="referral" required="" type="text" class="form-control mt-1" placeholder="Nhập mã giới thiệu">
            <span style="color: red; font-size: 12px; font-weight: bold;">
            </span> -->

            <span style="color: red; font-size: 12px; font-weight: bold;">
            </span>
            <center>
                <div class="g-recaptcha" data-sitekey="<?= $site_key; ?>"></div>
            </center>
            <div class="text-center mt-1">
                <button class="btn btn-lg btn-dark btn-block" style="border-radius: 10px;width: 100%; height: 50px;" type="submit" name="submit">Đăng ký</button>
            </div>
        </form>
    </div>
    <!-- <script>
        const urlParams = new URLSearchParams(window.location.search);
        const referralCode = urlParams.get('referral');
        let ipAddressCurrent = null;
        let ipAddressReferral = null;


        if (referralCode) {
            fetch('../Utils/getip.php?referral=' + encodeURIComponent(referralCode))
                .then(response => response.text())
                .then(ipAddressReferral => {
                    console.log(ipAddressReferral);

                    fetch('../Utils/getcurrentip.php')
                        .then(response => response.text())
                        .then(ipAddressCurrent => {
                            console.log(ipAddressCurrent);

                            if (ipAddressCurrent == ipAddressReferral) {
                                alert('Bạn không thể sử dụng mã giới thiệu của chính mình!');
                                window.location.href = "/pages/dangky.php";
                            }

                            const referral = document.querySelector('input[name="referral"]');
                            referral.value = referralCode;
                            referral.readOnly = true;
                        })
                        .catch(error => {
                            console.error('Error fetching current IP:', error);
                        });
                })
                .catch(error => {
                    console.error('Error fetching referral IP:', error);
                });
        }
        Sử dụng hàm getLocalIP để lấy địa chỉ IP và log ra kết quả
    </script> -->
</main>
<?php require_once('../core/end.php'); ?>