<?php
session_start();
require_once('../core/config.php');
require_once('../core/head.php');
require_once('../api/config.php');

if (isset($_POST['submit'])) {
}
?> <main>
  <style>
    .hidden {
      display: none;
    }
  </style>
  <div style="background: #ffe8d1; border-radius: 7px; box-shadow: 0px 2px 5px black;" class="pb-1">

    <form class="text-center col-lg-5 col-md-10" style="margin: auto;" method="POST" action="" id="myform">
      <h1 class="h3 mb-3 font-weight-normal">Nạp Momo Auto</h1>
      <label>Mệnh giá:</label>
      <select style="height: 50px; border-radius: 15px; font-weight: bold;" class="form-control form-control-alternative mt-1" name="momo_amount" required>
        <option value="">Chọn mệnh giá</option>
        <option value="10000">10.000</option>
        <option value="20000">20.000</option>
        <option value="30000">30.000 </option>
        <option value="50000">50.000</option>
        <option value="100000">100.000</option>
        <option value="200000">200.000</option>
        <option value="300000">300.000</option>
        <option value="500000">500.000</option>
        <option value="1000000">1.000.000</option>
      </select>
      <span style="color: red; font-size: 12px; font-weight: bold;"></span>
      <div class="text-center mt-5">
        <button class="btn btn-lg btn-dark btn-block" style="border-radius: 10px;width: 100%; height: 50px;" type="submit" name="submit">NẠP NGAY</button>
      </div>

      <div class="momo-container hidden">
        <!-- <div class="momo-qr">
          <img src="./img/momo.jpg" class="momo-img" alt="Chuyển khoản momo">
        </div> -->
        <label class="momo-content">Số điện thoại: </label><br>
        <div class="input-copy d-flex justify-content-between align-items-center">
          <input type="text" class="form-control form-control-alternative content-momo" style="height: 50px; border-radius: 15px; font-weight: bold; margin-right: 15px;" name="text-phone" value="<?php echo $momo; ?>" readonly required>
          <i class="fa fa-clipboard" onclick="copyTextContent2()"></i>
        </div>
        <label class="momo-content">Nội dung chuyển khoản </label><br>
        <div class="input-copy mt-2 d-flex justify-content-between align-items-center">
          <input type="text" class="form-control form-control-alternative content-momo" style="background-color: #DCDCDC; font-weight: bold; color: #696969;  margin-right: 15px;" name="text-content" value="Lỗi..." readonly required>
          <i class="fa fa-clipboard" onclick="copyTextContent()"></i>
        </div>
      </div>

      <div id="status-the" class="hidden">
        <div class="spinner-box" style="margin: 0;">
          <div class="configure-border-1">
            <div class="configure-core"></div>
          </div>
          <div class="configure-border-2">
            <div class="configure-core"></div>
          </div>
        </div>
      </div>
    </form>
    <br><br>
    <div>- Hãy Kiểm Tra Kĩ Thông Tin Trước Khi Nạp</div>
    <div>- Nạp sai nội dung vui lòng liên hệ admin. </div>
    <div>- Quá 30 Phút Chưa Duyệt Hãy Báo Ngay Cho Admin Để Được Hỗ Trợ Nhanh Nhất!</div>
  </div>

  <script type="text/javascript">
    var statusContent = $("#status").html();
    $("#myform").submit(function(e) {
      if ($("select[name*='momo_amount']")) {
        var momo_amount = $("select[name*='momo_amount']").val();
        if (momo_amount == "") {
          toastr.error("Vui Lòng Chọn Mệnh Giá !");
          return false;
        }
      }
      e.preventDefault();
      $.ajax({
        url: "../ajax/momo.php",
        type: 'post',
        data: $("#myform").serialize(),
        success: function(data) {
          console.log(data);
          console.log($("select[name*='text-content']").val());
          $("input[name*='text-content']").val(data);
        }
      });
      $(".momo-container").removeClass('hidden');
    });
  </script>

  <script>
    function copyTextContent() {
      var input = document.getElementsByName("text-content")[0];
      input.select();
      document.execCommand("copy");
      window.getSelection().removeAllRanges();
      toastr.success("Đã sao chép nội dung: " + input.value);
    }

    function copyTextContent2() {
      var input = document.getElementsByName("text-phone")[0];
      input.select();
      document.execCommand("copy");
      window.getSelection().removeAllRanges();
      toastr.success("Đã sao chép nội dung: " + input.value);
    }
  </script>
</main> <?php require_once('../core/end.php'); ?>