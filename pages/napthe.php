<?php
session_start();
require_once('../core/config.php');
require_once('../core/head.php');
require_once('../api/config.php');

if (isset($_POST['submit'])) {
  if (!isset($_POST['pin'], $_POST['serial'], $_POST['card_type'], $_POST['card_amount'])) {
    exit('<script type="text/javascript">toastr.error("Vui Lòng Nhập Đầy Đủ Thông Tin !");</script>');
  }

  $content = md5(time() . rand(0, 999999) . microtime(true));
  $seri = $config->real_escape_string(strip_tags(addslashes($_POST['serial'])));
  $pin = $config->real_escape_string(strip_tags(addslashes($_POST['pin'])));
  $loaithe = $config->real_escape_string(strip_tags(addslashes($_POST['card_type'])));
  $menhgia = $config->real_escape_string(strip_tags(addslashes($_POST['card_amount'])));
  $username = $config->real_escape_string(strip_tags(addslashes($_username))); // sử dụng biến phiên

  $url = "https://thesieutoc.net/chargingws/v2?APIkey=" . $apikey . "&mathe=" . $pin . "&seri=" . $seri . "&type=" . $loaithe . "&menhgia=" . $menhgia . "&content=" . $content;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_CAINFO, __DIR__ . '/../api/curl-ca-bundle.crt');
  curl_setopt($ch, CURLOPT_CAPATH, __DIR__ . '/../api/curl-ca-bundle.crt');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $response = curl_exec($ch);

  // Error handling for cURL
  if ($response === false) {
    echo '<script type="text/javascript">toastr.error("Có lỗi khi thực hiện yêu cầu: ' . curl_error($ch) . '");</script>';
    curl_close($ch);
    exit; // Stop execution
  }

  $out = json_decode($response);
  $http_code = 0;
  if (isset($out->status)) {
    $http_code = 200;
  }
  curl_close($ch);

  if ($http_code == 200) {
    if ($out->status == '00' || $out->status == 'thanhcong') {
      $config->query("Insert into trans_log (name,trans_id,amount,pin,seri,type) values ('" . $username . "','" . $content . "'," . $menhgia . ",'" . $pin . "','" . $seri . "','" . $loaithe . "')");
      echo '<script type="text/javascript">swal("Thành Công", "' . $out->msg . '", "success");</script>';
    } else if ($out->status != '00' && $out->status != 'thanhcong') {
      echo '<script type="text/javascript">toastr.error("' . $out->msg . '");</script>';
    }
  } else {
    echo '<script type="text/javascript">toastr.error("Có lỗi máy chủ vui lòng thử lại sau!");</script>';
  }
}
?> <main>
  <div style="background: #ffe8d1; border-radius: 7px; box-shadow: 0px 2px 5px black;" class="pb-1">

    <form class="text-center col-lg-5 col-md-10" style="margin: auto;" method="POST" action="" id="myform">
      <h1 class="h3 mb-3 font-weight-normal">Nạp Thẻ</h1>

      <label>Loại thẻ:</label>
      <select style="height: 50px; border-radius: 15px; font-weight: bold;" class="form-control form-control-alternative mt-1" name="card_type" required>
        <?php
        $cdurl = curl_init("https://thesieutoc.net/card_info.php");
        curl_setopt($cdurl, CURLOPT_FAILONERROR, true);
        curl_setopt($cdurl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($cdurl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cdurl, CURLOPT_CAINFO, __DIR__ . '/../api/curl-ca-bundle.crt');
        curl_setopt($cdurl, CURLOPT_CAPATH, __DIR__ . '/../api/curl-ca-bundle.crt');

        // Execute the cURL request and handle potential errors
        $response = curl_exec($cdurl);
        if ($response === false) {
          echo 'cURL Error: ' . curl_error($cdurl);
          curl_close($cdurl);
          exit; // Stop further execution
        }

        curl_close($cdurl);

        $obj = json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
          echo 'JSON Error: ' . json_last_error_msg();
          exit; // Stop further execution
        }

        $length = count($obj);
        for ($i = 0; $i < $length; $i++) {
          if ($obj[$i]['status'] == 1) {
            echo '<option value="' . $obj[$i]['name'] . '">' . $obj[$i]['name'] . ' (Nhận 100%)</option>';
          }
        }
        ?>
      </select>
      <span style="color: red; font-size: 12px; font-weight: bold;"></span>

      <label>Mệnh giá:</label>
      <select style="height: 50px; border-radius: 15px; font-weight: bold;" class="form-control form-control-alternative mt-1" name="card_amount" required>
        <option value="">Chọn mệnh giá</option>
        <option value="10000">10.000</option>
        <option value="20000">20.000</option>
        <option value="30000">30.000 </option>
        <option value="50000">50.000</option>
        <option value="100000">100.000</option>
        <option value="200000">200.000</option>
        <option value="300000">300.000</option>
        <option value="500000">500.000</option>
        <option value="1000000">1.000.000</option>
      </select>
      <span style="color: red; font-size: 12px; font-weight: bold;"></span>

      <label>Số seri:</label>
      <input style="height: 50px; border-radius: 15px; font-weight: bold;" type="text" class="form-control form-control-alternative mt-1" name="serial" required />
      <span style="color: red; font-size: 12px; font-weight: bold;"></span>

      <label>Mã thẻ:</label>
      <input style="height: 50px; border-radius: 15px; font-weight: bold;" type="text" class="form-control form-control-alternative mt-1" name="pin" required />
      <span style="color: red; font-size: 12px; font-weight: bold;"></span>

      <div class="text-center mt-5">
        <button class="btn btn-lg btn-dark btn-block" style="border-radius: 10px;width: 100%; height: 50px;" type="submit" name="submit">NẠP NGAY</button>
      </div>

      <div id="status-the" class="hidden">
        <div class="spinner-box" style="margin: 0;">
          <div class="configure-border-1">
            <div class="configure-core"></div>
          </div>
          <div class="configure-border-2">
            <div class="configure-core"></div>
          </div>
        </div>
      </div>
    </form>
    <br><br>
    <div>- Hãy Kiểm Tra Kĩ Thông Tin Trước Khi Nạp</div>
    <div>- Nạp Sai Mệnh Giá, Thông Tin Thẻ Admin Không Chịu Trách Nhiệm.</div>
    <div>- Quá 30 Phút Thẻ Chưa Duyệt Hãy Báo Ngay Cho Admin Để Được Hỗ Trợ Nhanh Nhất!</div>
  </div>

</main> <?php require_once('../core/end.php'); ?>