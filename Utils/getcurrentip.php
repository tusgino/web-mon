<?php

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
  $ip_address = $_SERVER['HTTP_CLIENT_IP'];
}
// Kiểm tra nếu địa chỉ IP từ proxy
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
  $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
// Kiểm tra nếu địa chỉ IP từ remote address
else {
  $ip_address = $_SERVER['REMOTE_ADDR'];
}

echo $ip_address;
