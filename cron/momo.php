<?php
include(__DIR__ . "/../api/config.php");

$url = "https://api.sieuthicode.net/historyapimomo/{$apikeymomo}";

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($curl);

// echo $response;

echo $heSo;

if ($response !== false) {
  // Xử lý phản hồi ở đây
  $jsonResponse = json_decode($response);
  if ($jsonResponse->status) {
    $tranList = $jsonResponse->momoMsg->tranList;

    $responseData = array(); // Khởi tạo mảng chứa dữ liệu

    foreach ($tranList as $tran) {
      $ID = $tran->ID;
      $tranId = $tran->tranId;
      $io = $tran->io;
      $partnerId = $tran->partnerId;
      $status = $tran->status;
      $partnerName = $tran->partnerName;
      $amount = $tran->amount;
      $comment = $tran->comment;
      $millisecond = $tran->millisecond;

      // Tạo một mảng chứa thông tin từng giao dịch
      $transactionData = array(
        "ID" => $ID,
        "tranId" => $tranId,
        "io" => $io,
        "partnerId" => $partnerId,
        "status" => $status,
        "partnerName" => $partnerName,
        "amount" => $amount,
        "comment" => $comment,
        "millisecond" => $millisecond
      );

      // Thêm thông tin giao dịch vào mảng chứa dữ liệu chung
      $responseData[] = $transactionData;

      $result = $conn->query("SELECT * FROM momo_trans WHERE status = 0 AND content = '{$comment}'");

      if ($result->num_rows > 0) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        if ($amount >= $row['amount']) {
          $price = $amount * $heSo;
          $conn->query("UPDATE account SET vnd = vnd + {$price}, tongnap = tongnap + {$price}, tichnap = tichnap + {$price} WHERE username = '{$row['username']}'");
          $conn->query("UPDATE momo_trans SET status = 1, trans_id = '{$tranId}' WHERE id = {$row['id']}");
        } else {
          $conn->query("UPDATE momo_trans SET status = 2 WHERE id = {$row['id']}");
        }
      }
    }

    // Chuyển đổi mảng chứa dữ liệu thành chuỗi JSON
    $jsonResponseData = json_encode($responseData);

    // Thiết lập các thông tin phản hồi HTTP
    header("Content-Type: application/json");
    echo $jsonResponseData;
  } else {
    // Xử lý khi có lỗi trong phản hồi API
    echo "Lỗi trong phản hồi API";
  }
} else {
  // Xử lý khi không thể lấy được phản hồi từ API
  echo "Có lỗi xảy ra khi gọi API.";
}

curl_close($curl);
