<!-- footer -->
<footer class="mt-1">
  <br>
  <div class="text-center text-black">
    <script>
      function getYear() {
        var date = new Date();
        var year = date.getFullYear();
        document.getElementById("currentYear").innerHTML = year;
      }
    </script>

    <body onload="getYear()">
      <small>
        <b><?= $ten_server; ?></b>
      </small>
      <br>
      <small>
        <span id="currentYear"></span> © Modified and fixed by Tú GiNô. <b>
          <u>Tú GiNô</u>
        </b>
      </small>
    </body>
  </div>
</footer>
</div>
</body>

</html>