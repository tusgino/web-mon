<?php
require_once('core/config.php');
require_once('core/head.php');
?>
<main>
  <div class="p-1 mt-1 ibox-content" style="border-radius: 7px; box-shadow: 0px 0px 5px black;">
    <br>
    <main>
      <h1 class="h5 mb-3 font-weight-normal">
        <b>
          <u>
            <center>Bảng Xếp Hạng TOP Đại Gia</center>
          </u>
        </b>
      </h1>
      <div class="table-responsive">
        <div style="line-height: 15px;font-size: 12px;padding-right: 5px;margin-bottom: 8px;padding-top: 2px;" class="text-center">
          <span class="text-black" style="vertical-align: middle;">Cập nhật 5 phút 1 lần</span>
        </div>
        <table class="table table-hover table-nowrap">
          <tbody style="border-color: black;">
            <tr>
              <th scope="col">Top</th>
              <th scope="col">Nhân vật</th>
              <th scope="col">Số Tiền</th>
            </tr>
            <?php
            $query = "SELECT name, tongnap FROM player PL JOIN account AC ON PL.account_id = AC.id WHERE ac.is_admin != 1 GROUP BY name ORDER BY tongnap DESC LIMIT 10";
            $result = $config->query($query);
            $stt = 1;
            if ($result === false) {
              echo 'Lỗi truy vấn SQL: ' . $config->error;
            } elseif ($result->num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                echo '<tr>
                              <td><b>#' . $stt . '</b></td>
                              <td>' . $row['name'] . '</td>
                              <td>' . number_format($row['tongnap']) . '<sup>đ</sup></td>
                            </tr>';
                $stt++;
              }
            } else {
              echo ' <tr>
                              <td colspan="3" align="center"><span style="font-size:100%;"><< Lịch Sử Trống >></span></td>
                            </tr>';
            }
            ?>
          </tbody>
        </table>
      </div>
      <h1 class="h5 mb-3 font-weight-normal">
        <b>
          <u>
            <center>Bảng Xếp Hạng TOP SỨC MẠNH</center>
          </u>
        </b>
      </h1>
      <div class="table-responsive">
        <div style="line-height: 15px;font-size: 12px;padding-right: 5px;margin-bottom: 8px;padding-top: 2px;" class="text-center">
          <span class="text-black" style="vertical-align: middle;">Cập nhật 5 phút 1 lần</span>
        </div>
        <table class="table table-hover table-nowrap">
          <tbody style="border-color: black;">
            <tr>
              <th scope="col">Top</th>
              <th scope="col">Nhân vật</th>
              <th scope="col">Sức mạnh</th>
            </tr>
            <?php
            $query = "SELECT name, CAST( split_str(data_point,',',2) AS UNSIGNED) AS sm FROM player PL JOIN account AC ON PL.account_id = AC.id WHERE ac.is_admin != 1 GROUP BY name ORDER BY CAST( split_str(data_point,',',2)  AS UNSIGNED) DESC LIMIT 10;";
            $result = $config->query($query);
            $stt = 1;
            if ($result === false) {
              echo 'Lỗi truy vấn SQL: ' . $config->error;
            } elseif ($result->num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                echo '<tr>
                              <td><b>#' . $stt . '</b></td>
                              <td>' . $row['name'] . '</td>
                              <td>' . number_format($row['sm']) . '</td>
                            </tr>';
                $stt++;
              }
            } else {
              echo ' <tr>
                              <td colspan="3" align="center"><span style="font-size:100%;"><< Lịch Sử Trống >></span></td>
                            </tr>';
            }
            ?>
          </tbody>
        </table>
      </div>
      <h1 class="h5 mb-3 font-weight-normal">
        <b>
          <u>
            <center>Bảng Xếp Hạng TOP NHIỆM VỤ</center>
          </u>
        </b>
      </h1>
      <div class="table-responsive">
        <div style="line-height: 15px;font-size: 12px;padding-right: 5px;margin-bottom: 8px;padding-top: 2px;" class="text-center">
          <span class="text-black" style="vertical-align: middle;">Cập nhật 5 phút 1 lần</span>
        </div>
        <table class="table table-hover table-nowrap">
          <tbody style="border-color: black;">
            <tr>
              <th scope="col">Top</th>
              <th scope="col">Nhân vật</th>
              <th scope="col">Số nhiệm vụ chính</th>
              <th scope="col">Số nhiệm vụ phụ 1</th>
              <th scope="col">Số nhiệm vụ phụ 2</th>
            </tr>
            <?php
            $query = "SELECT name, CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(data_task, ',', 1), '[', -1) AS UNSIGNED) AS task1, CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(data_task, ',', 2), ',', -1) AS UNSIGNED) AS task2, CAST(SUBSTRING_INDEX(data_task, ',', -1) AS UNSIGNED) AS task3, CAST(split_str(data_point,',',2) AS UNSIGNED) AS data_point FROM player PL JOIN account AC ON PL.account_id = AC.id  WHERE AC.is_admin != 1  ORDER BY task1 DESC, task2 DESC, task3 DESC, data_point DESC LIMIT 20;";
            $result = $config->query($query);
            $stt = 1;
            if ($result === false) {
              echo 'Lỗi truy vấn SQL: ' . $config->error;
            } elseif ($result->num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                echo '<tr>
                              <td><b>#' . $stt . '</b></td>
                              <td>' . $row['name'] . '</td>
                              <td>' . number_format($row['task1']) . '</td>
                              <td>' . number_format($row['task2']) . '</td>
                              <td>' . number_format($row['task3']) . '</td>
                            </tr>';
                $stt++;
              }
            } else {
              echo ' <tr>
                              <td colspan="3" align="center"><span style="font-size:100%;"><< Lịch Sử Trống >></span></td>
                            </tr>';
            }
            ?>
          </tbody>
        </table>
      </div>
    </main>
  </div>
</main>
<div class="modal right fade" id="Noti_Home" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-mdb-backdrop="static" data-mdb-keyboard="true">
  <div class="modal-dialog modal-side modal-bottom-right ">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #2c2c2c; color: #FFF; text-align: center;">
        <img src="../hoangvietdung_public/images/logo/logo.png" style="display: block; margin-left: auto; margin-right: auto; max-width: 150px;">
      </div>
      <div class="modal-body">
        <center>
          <p style="padding: 10px">
            <b style="color:red"><u>Thông Báo</u></b><br>
            Tham gia <?= $ten_server; ?> trên các nền tảng mạng xã hội nhé.!<br><br>
            <a href="<?= $box_zalo; ?>" class="btn btn-download" style="border-radius: 10px; color: #FFFFFF;" target="_blank"><b>Box Zalo</b></a>
          </p>
        </center>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#Noti_Home').modal('show');
  })
</script>
<?php require_once('core/end.php'); ?>